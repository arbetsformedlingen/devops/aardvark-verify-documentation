FROM docker.io/library/ubuntu:24.04

WORKDIR /data
COPY . /data

RUN apt-get -y update &&\
    # this for ubuntu when theres no default python
    apt-get -y install python3 python3-pip python3-venv &&\
    python3 -m venv venv && . venv/bin/activate && pip install -r requirements.txt &&\
    echo "hello"

EXPOSE 5000
ENV NAME country

CMD . venv/bin/activate && python3 app.py
